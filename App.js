import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Button, Image } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';

export default function App() {
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);
  const [product, setProduct]=useState({
    name:'',
    nutriscore: '',
    picture: ''
  })

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  const handleBarCodeScanned = ({ data }) => {
    setScanned(data);
   fetch('https://world.openfoodfacts.org/api/v0/product/'+data+'.json')
   .then(response => response.json())
   .then(data => {
     if(data.status){
     setProduct({
      name:data.product.product_name,
      nutriscore:data.product.nutriscore_grade,
      picture:data.product.image_url
     })
     }
   })
  };

  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return (
    <View style={styles.container}>

{scanned &&
    <>
      <Text> Nom : {product.name}</Text>
      <Text> Nutriscore : {product.nutriscore}</Text>
      <Image source={{ uri:product.picture}} />
      </>
}
     
      {!scanned && 
      <>
       <BarCodeScanner
        onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
        style={StyleSheet.absoluteFillObject}
      />
      </>
      }
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
});
